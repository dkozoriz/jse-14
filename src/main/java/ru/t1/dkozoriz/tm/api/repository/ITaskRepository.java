package ru.t1.dkozoriz.tm.api.repository;

import ru.t1.dkozoriz.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IAbstractRepository<Task> {
    List<Task> findAllByProjectId(String ProjectId);

}