package ru.t1.dkozoriz.tm.service;

import ru.t1.dkozoriz.tm.api.repository.IProjectRepository;
import ru.t1.dkozoriz.tm.api.repository.ITaskRepository;
import ru.t1.dkozoriz.tm.api.service.IProjectTaskService;
import ru.t1.dkozoriz.tm.model.Project;
import ru.t1.dkozoriz.tm.model.Task;

import java.util.List;

public class ProjectTaskService implements IProjectTaskService {

    private final IProjectRepository projectRepository;

    private final ITaskRepository taskRepository;

    public ProjectTaskService(final IProjectRepository projectRepository, final ITaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    public Task bindTaskToProject(final String projectId, final String taskId) {
        if (projectId == null || projectId.isEmpty()) return null;
        if (taskId == null || taskId.isEmpty()) return null;
        final Project project = projectRepository.findOneById(projectId);
        if (project == null) return null;
        final Task task = taskRepository.findOneById(taskId);
        if (task == null) return null;
        task.setProjectId(projectId);
        return task;
    }

    public void removeProjectById(final String projectId) {
        if (projectId == null || projectId.isEmpty()) return;
        final Project project = projectRepository.findOneById(projectId);
        if (project == null) return;
        final List<Task> tasks = taskRepository.findAllByProjectId(projectId);
        for (final Task task : tasks) taskRepository.removeById(task.getId());
        projectRepository.removeById(projectId);
    }

    public Task unbindTaskToProject(final String projectId, final String taskId) {
        if (projectId == null || projectId.isEmpty()) return null;
        if (taskId == null || taskId.isEmpty()) return null;
        final Project project = projectRepository.findOneById(projectId);
        if (project == null) return null;
        final Task task = taskRepository.findOneById(taskId);
        if (task == null) return null;
        task.setProjectId(null);
        return task;
    }

}